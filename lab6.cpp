#include <iostream>
using namespace std;

class array_out_of_bounds {};

template <typename T>

class Kit
{
protected:
	virtual void push(T number) = 0;
	virtual void print() = 0;  
	virtual void press() = 0;
};

template <typename T> 

class Vector : public Kit <T>
{
private:
	int size;
	T *mass;
	T begin;
	Vector *next;
	Vector *previous;
public:
	Vector()
	{
		size = 5;
		mass = new T[size];
	}

	void push(T number) override
	{
		Vector *lst = this;
		Vector *temp = this, *p;
		p = lst->next;
		lst->next = temp;
		temp->begin = number;
		temp->next = p;
		temp->previous = lst;

		if (p != NULL)
			p->previous = temp;
	}

	void print()
	{
		Vector *lst = this;
		Vector *p;
		p = lst;
		do {
			cout << p->begin << "  ";
			p = p->next;
		} while (p != NULL);
	}

	void press()
	{
		for (int i = 0; i < size; i++)
		{
			cout << mass[i] << " ";
		}
		cout << endl << endl;
	}
	
	T &operator[](int i)
	{
		if (i >= size || i < 0) {
			throw array_out_of_bounds();
		}
			return mass[i];
	}
	
	
	int add_elem(int choice, T choice1)
	{
		size++;
		if (choice >= size || choice < 0)
			return -1;
		for (int i = size - 1; i >= choice - 1; i--)
			mass[i + 1] = mass[i];
		mass[choice] = choice1;
		return 0;
	}
	
	int find(T elem)
	{
		int i;
		for (i = 0; i < size; i++) {
			if (mass[i] == elem)
				return i;
		}
		return -1;
	}

	bool Del(int nom)
	{
		if (nom >= size || nom < 0)
		{
			cout << "Error" << endl;
			return false;
		}
		for (int ix = nom - 1; ix < size - 1; ix++)
		{
			mass[ix] = mass[ix + 1];
		}
		size--;
		return true;
	}
};

int main()
{
	int choice;
	while (1) {
		cout <<"[1] Int [2] Char\n";
		cin >> choice;
		if (choice == 1) {
			int mass[] = { 1, 2, 3, 4, 5 };
			
			Vector <int> *vector = new Vector <int>;

			for (int i = 0; i < 5; ++i) {
				vector->push(mass[i]);
				vector->print();
				cout << endl;
			}

			Vector <int> Array;
			for (int i = 0; i < 5; ++i) {
				try {
					Array[i] = mass[i];
				}
				catch (array_out_of_bounds) {
					cout << "Array out of bounds" << endl;
					break;
				}
			}
			cout << "Array:  ";
			Array.press();

			int choice1;
			cout << "Enter the position of the element in the array and the element you want to add:\n";
			cin >> choice >> choice1;

			int s = Array.add_elem(choice, choice1);
			if (s != -1)
				Array.press();
			else
				cout << "Invalid index" << endl;

			cout << "Enter the item you want to search for: ";
			cin >> choice1;
			int q = Array.find(choice1);

			if (q != -1)
				cout << "Index of the searched element: " << q << endl;
			else
				cout << "Not found" << endl;

			cout << "Specify the number of the array element you want to delete:\n";
			cin >> choice1;
			Array.Del(choice1);
			Array.press();
		}

		else if (choice == 2) {
			char mass[] = { 'a', 'b', 'c', 'd', 'e' };
			Vector <char> *v = new Vector <char>;

			cout << "Vector" << endl;
			for (int i = 0; i < 5; ++i) {
				v->push(mass[i]);
				v->print();
				cout << endl;
			}
			Vector <char> Array;
			for (int i = 0; i < 5; ++i) {
				try {
					Array[i] = mass[i];
				}
				catch (array_out_of_bounds) {
					cout << "Array out of bounds" << endl;
					break;
				}
			}
			cout << "Array:  ";
			Array.press();

			char choice1;
			cout << "Enter the position of the element in the array and the element you want to add:\n";
			cin >> choice >> choice1;

			int s = Array.add_elem(choice, choice1);
			if (s != -1)
				Array.press();
			else
				cout << "Invalid index" << endl;

			cout << "Enter the item you want to search for: ";
			cin >> choice1;
			cin.get();

			int q = Array.find(choice1);
			if (q != -1)
				cout << "Index of the searched element: " << q << endl;
			else
				cout << "Not found" << endl;

			cout << "Specify the number of the array element you want to delete:\n";
			cin >> choice;
			Array.Del(choice);
			Array.press();
		}
	}
}
